const products = [
    {
        title: "F-lite crew sock",
        price: 14.00,
        currency: "£",
        colors: [
            {
                color: "White/Black", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000934-WHBK-01-F-Lite-Crew-Sock-White-Black-1.jpg",
                sizes: [
                    { size: "S", instock:  true },
                    { size: "M", instock:  true},
                    { size: "L", instock: false}
                ]
            },
            {
                color: "Black/Camo", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000934-BKCA-01-F-Lite-Crew-Sock-Black-Khaki-Camo-1.jpg",
                sizes: [
                    { size: "S", instock:  false },
                    { size: "M", instock:  false},
                    { size: "L", instock: false}
                ]
            }
        ]
    },
    {
        title: "Softflask 0.25L",
        price: 14.00,
        currency: "£",
        colors: [
            {
                color: "Clear/Black", 
                image: "https://www.inov-8.com/media/catalog/product/cache/69fcd86dd61471de811ac25f32c509f9/s/o/softflask.jpg",
                sizes: []
            }
        ],
    },
    {
        title: "TRAILFLY ULTRA G 300 MAX MEN'S",
        price: 170.00,
        currency: "£",
        colors: [
            {
                color: "Blue/Black", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000977-BLBK-S-01-TRAILFLY-ULTRA-G-300-M-BLUE-BLACK-1.jpg",
                sizes: [
                    { size: "6", instock: false },
                    { size: "6.5", instock: true},
                    { size: "7", instock: false},
                    { size: "7.5", instock: false},
                    { size: "8", instock:  false},
                    { size: "8.5", instock: false},
                    { size: "9", instock:  true},
                    { size: "9.5", instock:  false},
                    { size: "10", instock:  false},
                    { size: "10.5", instock: false},
                    { size: "11", instock:  false},
                    { size: "11.5", instock:  false},
                    { size: "12", instock:  true},
                    { size: "12.5", instock: true}
                ]
            },
            { 
                color: "Green/Black", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000977-GNBK-S-01-TRAILFLY-ULTRA-G-300-M-GREEN-BLACK-1.jpg",
                sizes: [
                    { size: "6", instock: true },
                    { size: "6.5", instock: false},
                    { size: "7", instock: true},
                    { size: "7.5", instock: false},
                    { size: "8", instock:  true},
                    { size: "8.5", instock: false},
                    { size: "9", instock:  false},
                    { size: "9.5", instock:  true},
                    { size: "10", instock:  false},
                    { size: "10.5", instock: false},
                    { size: "11", instock:  true},
                    { size: "11.5", instock:  false},
                    { size: "12", instock:  false},
                    { size: "12.5", instock: true}
                ]
            },
            {
                color: "Black/White/Green",
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000977-BKWHGR-S-01-TRAILFLY-ULTRA-G-300-M-BLACK-WHITE-GREEN-1.jpg",
                sizes: [
                    { size: "6", instock: false },
                    { size: "6.5", instock: false},
                    { size: "7", instock: true},
                    { size: "7.5", instock: false},
                    { size: "8", instock:  true},
                    { size: "8.5", instock: false},
                    { size: "9", instock:  true},
                    { size: "9.5", instock:  true},
                    { size: "10", instock:  true},
                    { size: "10.5", instock: false},
                    { size: "11", instock:  true},
                    { size: "11.5", instock:  true},
                    { size: "12", instock:  true},
                    { size: "12.5", instock: false}
                ] }
        ]
    },
    {
        title: "ROCLITE G 315 GTX MEN'S",
        price: 145.00,
        currency: "£",
        colors: [
            {
                color: "Grey/Orange", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000804-GYOR-P-01-ROCLITE-G-315-GTX-M-Grey-Orange-1.jpg",
                sizes: [
                    { size: "6", instock: false },
                    { size: "6.5", instock: false},
                    { size: "7", instock: false},
                    { size: "7.5", instock: false},
                    { size: "8", instock:  true},
                    { size: "8.5", instock:  true},
                    { size: "9", instock:  true},
                    { size: "9.5", instock:  true},
                    { size: "10", instock:  true},
                    { size: "10.5", instock:  true},
                    { size: "11", instock: false},
                    { size: "11.5", instock: false},
                    { size: "12", instock: false},
                    { size: "12.5", instock: false}
                ]},
            { 
                color: "Brown/Red", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000804-BRRD-M-01-Roclite-G-315-GTX-M-Brown-Red-1.jpg",
                sizes: [
                    { size: "6", instock: true },
                    { size: "6.5", instock: false},
                    { size: "7", instock: false},
                    { size: "7.5", instock: false},
                    { size: "8", instock:  false},
                    { size: "8.5", instock:  false},
                    { size: "9", instock:  true},
                    { size: "9.5", instock:  true},
                    { size: "10", instock:  false},
                    { size: "10.5", instock:  false},
                    { size: "11", instock: false},
                    { size: "11.5", instock: false},
                    { size: "12", instock: true},
                    { size: "12.5", instock: true}
                ]},
            { 
                color: "Navy/Yellow", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000804-NYYW-M-01-Roclite-G-315-GTX-W-Navy-Yellow-1.jpg",
                sizes: [
                    { size: "6", instock: false },
                    { size: "6.5", instock: false},
                    { size: "7", instock: true},
                    { size: "7.5", instock: false},
                    { size: "8", instock:  true},
                    { size: "8.5", instock:  false},
                    { size: "9", instock:  false},
                    { size: "9.5", instock:  true},
                    { size: "10", instock:  false},
                    { size: "10.5", instock:  true},
                    { size: "11", instock: false},
                    { size: "11.5", instock: true},
                    { size: "12", instock: false},
                    { size: "12.5", instock: true}
                ] },
            { 
                color: "Black/Dark Green", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000804-BKDN-M-01-BLACK-GREEN-M-01.jpg",
                sizes: [
                    { size: "6", instock: false },
                    { size: "6.5", instock: false},
                    { size: "7", instock: true},
                    { size: "7.5", instock: true},
                    { size: "8", instock:  true},
                    { size: "8.5", instock:  false},
                    { size: "9", instock:  false},
                    { size: "9.5", instock:  false},
                    { size: "10", instock:  false},
                    { size: "10.5", instock:  true},
                    { size: "11", instock: false},
                    { size: "11.5", instock: true},
                    { size: "12", instock: true},
                    { size: "12.5", instock: true}
                ] },
            { 
                color: "Olive/Black/Red", 
                image: "https://www.inov-8.com/media/catalog/product/cache/111a8af1e99ed4b2d5fcd19443eb9ee7/0/0/000804-OLBKRD-M-01-OLIVE-BLACK-RED-M-1.jpg",
                sizes: [
                    { size: "6", instock: false },
                    { size: "6.5", instock: true},
                    { size: "7", instock: false},
                    { size: "7.5", instock: false},
                    { size: "8", instock:  true},
                    { size: "8.5", instock:  true},
                    { size: "9", instock:  true},
                    { size: "9.5", instock:  true},
                    { size: "10", instock:  true},
                    { size: "10.5", instock:  true},
                    { size: "11", instock: false},
                    { size: "11.5", instock: false},
                    { size: "12", instock: false},
                    { size: "12.5", instock: false}
                ] }
        ]
    }
]

const cartSlider = document.querySelector('.cartSlider');




let countHovers = 0;
let currentColor = '';
let currentSize = null;

const createHtml = (array, quickAddFunction) => {

    array.forEach((product, index) => {

        let productDiv = document.createElement('div');
        productDiv.classList.add('cartSlider__card');
        productDiv.setAttribute("id", `productCard${index}`);
        cartSlider.appendChild(productDiv);

     

        let cartSliderImage = document.createElement('div');
        cartSliderImage.classList.add('cartSlider__card__image');
        cartSliderImage.innerHTML =  `<img src=${product.colors[0].image} />`;
        productDiv.appendChild(cartSliderImage);

        let cartSliderMeta = document.createElement('div');
        cartSliderMeta.classList.add('cartSlider__card__meta');
        productDiv.appendChild(cartSliderMeta);

        let cartSliderMetaTitle = document.createElement('div');
        cartSliderMetaTitle.classList.add('cartSlider__card__details');
        cartSliderMetaTitle.innerHTML = `<div class="cartSlider__card__meta__title">
                                                <p>${product.title}</p>
                                                <p>£${product.price}</p>
                                            </div>`;
        cartSliderMeta.appendChild(cartSliderMetaTitle);

        let cartSliderMetaColours = document.createElement('div');
        cartSliderMetaColours.classList.add('cartSlider__card__meta__colours');
        cartSliderMetaColours.innerHTML = `<p>${product.colors.length} ${product.colors.length > 1? 'colours': 'colour'}</p>`;
        cartSliderMetaTitle.appendChild(cartSliderMetaColours);

        let cartSliderMetaCTA = document.createElement('div');
        cartSliderMetaCTA.classList.add('cartSlider__card__meta__cta');
        cartSliderMeta.appendChild(cartSliderMetaCTA);

        let cartSliderMetaButton = document.createElement('button');
        cartSliderMetaButton.classList.add('quickAdd');
        cartSliderMetaButton.innerText = `Quick Add`;
        cartSliderMetaCTA.appendChild(cartSliderMetaButton);

        cartSliderMetaButton.onclick = () => {
            quickAddFunction(product.colors[0].color, "N/A");
        }
      
        let storedIndex = countHovers;

        if( product.colors.length > 1){
     
        countHovers++;

        cartSliderMetaButton.onmouseover = (e) => {
            cartSliderMetaHover.classList.remove('displayNone');
            currentColor = product.colors[0].color;
        }

       let cartSliderMetaHover = document.createElement('div');
        cartSliderMetaHover.classList.add('cartSlider__card__hover');
        cartSliderMetaHover.classList.add('displayNone');
        productDiv.appendChild(cartSliderMetaHover); 

        cartSliderMetaHover.onmouseleave = (e) => {
       

            cartSliderMetaHover.classList.add('displayNone');
           
            const quickAddHover = document.querySelectorAll('.quickAddHover');
            quickAddHover.forEach(button => button.classList.add('disabled'));

            const getDiv = document.querySelectorAll('.card__hover__colour__colours div');
          

        } 

        let cartSliderMetaHoverMeta = document.createElement('div');
        cartSliderMetaHoverMeta.classList.add('card__hover__meta');
        cartSliderMetaHoverMeta.innerHTML = ` <p>${product.title}</p><p>£${product.price}</p>`;
        cartSliderMetaHover.appendChild(cartSliderMetaHoverMeta);

        let cartSliderMetaHoverColour = document.createElement('div');
        cartSliderMetaHoverColour.classList.add('card__hover__colour');
        cartSliderMetaHoverColour.innerHTML = `<p class="card__hover__colour__ident">${product.colors[0].color}</p>`;
        cartSliderMetaHover.appendChild(cartSliderMetaHoverColour);

        let cartSliderMetaHoverColourDiv = document.createElement('div');
        cartSliderMetaHoverColourDiv.classList.add('card__hover__colour__colours');
        cartSliderMetaHover.appendChild(cartSliderMetaHoverColourDiv);
      
        if(product.colors.length > 0){

            let cartSliderMetaHoverSizeDiv;


    let cartSliderMetaHoverSize = document.createElement('div');
    cartSliderMetaHoverSize.classList.add('card__hover__size');
    cartSliderMetaHoverSize.innerHTML = `<p class="card__hover__size__ident">Size</p>`;
    cartSliderMetaHover.appendChild(cartSliderMetaHoverSize);

    cartSliderMetaHoverSizeDiv = document.createElement('div');
    cartSliderMetaHoverSizeDiv.classList.add('card__hover__size__sizeCards');
    cartSliderMetaHoverSize.appendChild(cartSliderMetaHoverSizeDiv);

            product.colors.forEach((color, indexColor) => {

                let div = document.createElement('div');
                let img = document.createElement('img');
                img.setAttribute("src", `${color.image}`);
                if(indexColor === 0){
                    div.classList.add('active');
                }
                div.appendChild(img);

                cartSliderMetaHoverColourDiv.appendChild(div);

                
                div.onclick = (e) => {
              
                    if(!div.classList.contains('active')){
                        const cartSliderCardImage = document.querySelectorAll('.cartSlider__card__image');
                
                        createSizesWhenSwitchingColors(product.colors[indexColor].sizes, cartSliderMetaHoverSizeDiv);

                        cartSliderCardImage[index].innerHTML = `<img src=${product.colors[indexColor].image}>`;
                        const cardHoverColourIdent = document.querySelectorAll('.card__hover__colour__ident');

                        cardHoverColourIdent[storedIndex].innerText = color.color;
                        currentColor = color.color;
                        const getDiv = document.querySelectorAll('.card__hover__colour__colours div');

                    getDiv.forEach(div => {
                        
                        const quickAddHover = document.querySelectorAll('.quickAddHover');
                            quickAddHover.forEach(button => {
                                button.classList.add('disabled');
                            });
                            if(e.target.parentNode.parentNode.parentNode.parentNode.id === div.parentElement.parentElement.parentElement.id){
                                div.classList.remove('active');
                            } 
                        });
                        div.classList.add('active');
                    }
                }  

            });


            createSizesWhenSwitchingColors(product.colors[0].sizes, cartSliderMetaHoverSizeDiv);
        }

     
        let cartSliderMetaHoverButton = document.createElement('div');
        cartSliderMetaHoverButton.classList.add('card__hover__button');

        cartSliderMetaHover.appendChild(cartSliderMetaHoverButton);


        let cartSliderMetaHoverButtonButton = document.createElement('button');
        cartSliderMetaHoverButtonButton.classList.add('quickAddHover');
        cartSliderMetaHoverButtonButton.classList.add('disabled');
        cartSliderMetaHoverButtonButton.innerText = `Quick Add`;
        cartSliderMetaHoverButton.appendChild(cartSliderMetaHoverButtonButton);

        cartSliderMetaHoverButtonButton.onclick = () => {
            quickAddFunction(currentColor, currentSize);
        }


    } 

    });
}



const createSizesWhenSwitchingColors = (array, cartSliderMetaHoverSizeDiv) => {

  cartSliderMetaHoverSizeDiv.innerHTML = ''; 

    array.forEach(size => {
        let div = document.createElement('div');
        div.innerText = size.size;
        if(size.instock == false){
            div.classList.add('notInStock');
        }
        cartSliderMetaHoverSizeDiv.appendChild(div);

        div.onclick = () => {
            currentSize = size.size;
            if(!div.classList.contains('active')){
                const getDiv = document.querySelectorAll('.card__hover__size__sizeCards div');
                getDiv.forEach(div => div.classList.remove('active'));
             
                const quickAddHover = document.querySelectorAll('.quickAddHover');
             
                quickAddHover.forEach(button => {
                    button.classList.remove('disabled');
                })

                div.classList.add('active');
            }
        }
    });
}

createHtml(products, (color, size) => alert(`PRODUCT HAS BEEN ADDED TO YOUR CART. \n Colour: ${color}\n Size: ${size}.`))